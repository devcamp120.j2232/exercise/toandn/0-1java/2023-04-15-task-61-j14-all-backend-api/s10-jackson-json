package com.devcamp.jacksonjson.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

public class Item {
  public int id;
  public String itemName;
  // cách 1 không có gì
  // cách 2
  // @JsonManagedReference
  // cách 3 không có gì
  // cách 4
  @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
  public User owner;

  public Item(int id, String itemName, User owner) {
    this.id = id;
    this.itemName = itemName;
    this.owner = owner;
  }

}
