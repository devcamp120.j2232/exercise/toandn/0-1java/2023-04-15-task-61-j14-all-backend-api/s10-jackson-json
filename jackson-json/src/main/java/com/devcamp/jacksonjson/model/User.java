package com.devcamp.jacksonjson.model;

import java.util.*;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

public class User {
  public int id;
  public String name;
  // @JsonManagedReference cách 1
  // cách 2
  // @JsonBackReference
  // cách 3
  // @JsonIgnore
  // cách 4
  @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
  public List<Item> userItems;

  public User(int id, String name) {
    this.id = id;
    this.name = name;
    userItems = new ArrayList<>();
  }

  public void addItem(Item item) {
    this.userItems.add(item);
  }

}
